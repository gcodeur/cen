---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
Bienvenue sur mon blog ! J'ai décidé de m'appeler ici Parydro, mais vous ne saurez pas mon vrai nom.
Élève de terminale engagée pour les droits des lycéen·ne·s, passionée de programmation à mes heures perdues,
j'ai lancé ce blog alors que la pression grandit pour moi vis-à-vis de l'épreuve qui nous attend cette année : Parcoursup.
J'y partagerai ici des billets pour vous détailler tous les aspects de la plateforme que je trouve problématiques. Et vous allez le voir,
il y a de quoi faire !

*Il est préférable de lire les posts en commençant par le plus ancien.*
