---
layout: post
title:  "Conclusion et perspectives"
date:   2020-11-26 12:00:00 +0100
---
Bonjour tout le monde ! Bienvenue sur ce qui sera certainement le dernier post de ce blog. J'ai beaucoup apprécié écrire ces articles et vous partager mes résultats, sur des questions tant fondamentales que passionnantes à étudier. J'aimerais vous laisser ici quelques éléments de conclusion.

Parcoursup ne fait pas que reproduire les inégalités socio-économiques déjà existantes au lycée, mais il en génère de nouvelles par son inégale compréhension, par l'inégale anxiété qu'il génère. En tant que plateforme régulant l'accès à l'enseignement supérieur, Parcoursup ne fait pas qu'amplifier les inégalités déjà existantes, mais en crée de nouvelles, notamment en allongeant la durée du processus pour les élèves issus des milieux défavorisés, et en les amenant à terme à se diriger vers des établissements privés.

Je pense que ce travail aurait pu être plus significatif avec la prise en compte de certains facteurs. Déjà, nous sommes encore très tôt dans l'année, ce qui peut expliquer le manque de connaissance de la plateforme chez les terminales. De plus, je n'ai pas pu analyser les PCS des parents de façon rigoureuse, et je pense que ma typologie entre les différents types de lycées aurait pu être améliorée. Je manque aussi de recul sur le nouveau bac, et les spécialités choisies par les terminales ont peut être aussi un intérêt que je n'ai pas pu mesurer. À ma décharge, c'est la première année que les élèves passés par cette réforme accèdent à Parcoursup, et mon échantillon de terminales restait trop restreint pour obtenir des statistiques pertinentes relatives aux spécialités. Enfin, je pense aussi que la pandémie de Covid-19 que nous traversons a pu agir comme une caisse de résonnance et amplifier les problèmes d'anxiété qu'ont dû traverser les lycéens l'année dernière avec la procédure, et le rôle de ce facteur supplémentaire n'aurait pas pu être mesuré avec précision que sur un travail de plus longue durée.

Pour peaufiner mes résultats, j'aurais aimer conduire des entretiens avec des lycéens en face de moi, mais cela n'a pas été possible avec le contexte pandémique. Je sais qu'un sondage Google Form n'est pas forcément la meilleure façon pour pouvoir analyser des pratiques, mais je pense que cela peut donner un panorama plutôt intéressant.

Je tiens à remercier toutes les personnes m'ayant soutenu dans ce projet d'écriture sur ces deux mois. Tous mes lecteurs·ices, ainsi que les personnes ayant accepté de répondre au questionnaire soumis, ou de s'adonner à un entretien sociologique. Votre participation a été extrêmement précieuse.

Je vous souhaite une excellente continuation, ainsi que beaucoup de succès à tou·te·s les terminales qui passeront par Parcoursup cette année.
