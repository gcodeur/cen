---
layout: post
title:  "Compréhension de Parcoursup, anxiété et lien avec la méritocratie"
date:   2020-10-30 12:00:00 +0100
---
Salut à tous ! J’espère que vous allez bien. Aujourd’hui, on va parler d’un sujet pas forcément joyeux, mais que je pense important : le lien entre anxiété et milieu social.

{:refdef: style="text-align: center;"}
![Anxiété selon compréhension]({{ "/assets/graphs/anxiete_selon_comprehension.png" | relative_url }})
{: refdef}

Sur le graphique ci-dessus, on voit que plus la procédure apparait compréhensible, moins les étudiants ayant expérimenté la procédure ont éprouvé de l’anxiété. On peut mettre aussi en relation avec le fait que plus la procédure Parcoursup s’est étirée dans la durée, plus les lycéens ont ressenti de l’anxiété.

{:refdef: style="text-align: center;"}
![Anxiété selon le nombre de jours]({{ "/assets/graphs/anxiete_selon_nb_jours.png" | relative_url }})
{: refdef}

Les deux facteurs sont corrélés, et défavorables aux lycéens issus de milieux défavorisés. Parcoursup génère de l’anxiété par sa durée et l’incertitude devant laquelle il confronte tant les élèves que leurs familles, en particulier celles n’ayant pas forcément énormément de moyens. Dans les réponses à mon questionnaire, une bachelière issue de REP m’a dit que :

> La peur d’être admise nulle part été un vrai stress non seulement pour moi mais aussi pour ma famille. Si je n’étais pas admise près de chez moi il fallait me trouver un logement ce qui n’est pas possible financièrement.

On voit aussi que la majorité des lycéen·ne·s a ressenti de l’anxiété face à Parcoursup. Il y a aussi une corrélation très nette entre l’anxiété ressentie par les lycéen·ne·s suivant leur lycée d’origine, ceux de REP ayant ressenti une anxiété très supérieure à celle des lycéen·ne·s issu·e·s de lycées privés sous contrat et de lycées publics. Un résultat intéressant de ce graphique concerne le fait que, pour les lycées publics catégorisés comme « d’excellence », c’est-à-dire sélectifs et socialement situés (comme Lakanal à Sceaux, Henri IV, Louis le Grand, Turgot à Paris, etc.), une très faible proportion des lycéen·ne·s n’a pas ressenti d’anxiété face à Parcoursup. Cela peut s’expliquer aussi par la pression du cercle familial à la réussite, donc aux effets de compétition dont j’ai parlé plus haut, car comme le montre le graphique ci-dessous (que je vous avais déjà montré), ces lycéen·ne·s sont anxieux·ses alors qu'on sait qu'iels comprennent la procédure ; leur anxiété n’est pas liée à leur incompréhension de la procédure.

{:refdef: style="text-align: center;"}
![Anxiété selon le lycée]({{ "/assets/graphs/anxiete_selon_lycee.png" | relative_url }})
{: refdef}

Cette anxiété est donc liée en partie à l’absence de compréhension, qui est elle socialement plus située, mais également à d’autres facteurs comme je l’ai lu dans les articles, notamment la totale compréhension de la plateforme qui est finalement vectrice d’anxiété. En effet, le fait de savoir sur quels critères on va être évalué met une pression d’autant plus forte. Beaucoup de vos témoignages vont dans ce sens. La pression étant plus forte chez ceux venant d’un milieu plus favorisé, elle est je pense à la fois corrélée à la compréhension de Parcoursup, et la pression des cellules institutionnelle et familiale.

> J'avais l'impression de ne jamais être à la hauteur, rien ne me semblait être assez bien, j'ai pleuré plusieurs fois face à des notes que j'avais reçues et qui n'était objectivement pas catastrophiques, juste pas à la hauteur face à la pression qui pesait sur mes épaules.

> Parcoursup a augmenté ma peur de l'échec. Ainsi tout résultat non satisfaisant me plombait le moral pour la semaine. Aussi le stress de l'échec n'a pas aidé en ma confiance en moi dans les mathématiques.

Cette anxiété croissante en fonction de la durée de la procédure a aussi un autre impact : la montée en puissance de l’enseignement supérieur privé. Désormais, selon l’article de 2020[^1], un·e étudiant·e sur cinq dans le supérieur est scolarisé·e dans un établissement privé. En jouant sur la montée de l’anxiété et l’angoisse de ne pas avoir de formation, ces établissements participent à des salons de l’orientation fin mai ou courant juin (voir même en juillet), ce qui va de pair avec une communication extrêmement active, ciblée et agressive sur les réseaux sociaux. Ce que me disait aussi A., quand je lui demandais si elle pensait que les activités extra-scolaires étaient prises en compte sur Parcoursup, c’est que :

> Je connais quelqu’un qui avait 11 de moyenne et qui a eu une école d’ingénieur sur Parcoursup parce qu’ils ont regardé autre chose que ses notes.

Ce qui est intéressant, c’est que les papiers sur Parcoursup s’accordent à dire que l’extra-scolaire n’est regardé qu’à la marge par les formations universitaires par exemple, mais est très considéré par les formations privées (qu’elles soient d’excellence ou non). Ces formations s’attachent davantage à recruter des élèves qu’à en recruter des bons, comme ce ne sont que des entreprises finalement, et que le problème venait du manque de contrôle vis-à-vis de ces formations, qui, souvent, ne délivraient que des diplômes non reconnus par l’Etat.

Ainsi, une conséquence sociale du maque de compréhension de Parcoursup et de l’étalement de la procédure d’affectation est cette montée de l’enseignement privé ; or, les plus susceptibles de se faire avoir par ces formations sont ceux qui voient leur procédure d’affectation s’allonger, cf. le tableau ci-dessous (un lycéen de REP a dû attendre 58 jours pour avoir une proposition d’affectation !) et qui sont, en général celleux issu·e·s de classes sociales défavorisées, et dont les parents sont en plus moins informés ! Parcoursup génère donc de l’anxiété pour tout le monde, mais pas pour les mêmes raisons, mais surtout accroit les inégalités sociales, en ne permettant pas à tous d’accéder au supérieur !

| Type de lycée | Temps moyen d'attente (jours) |
| ---------- | ----------- |
| Lycée public d’excellence | 19 |
| Lycée privé sous contrat | 34 |
| Lycée public (hors REP) | 30 |
| Lycée public REP | 40 |

[^1]: Frouillou Leïla, Pin Clément, van Zanten Agnès, « Les plateformes APB et Parcoursup au service de l’égalité des chances ? L’évolution des procédures et des normes d’accès à l’enseignement supérieur en France », *L'Année sociologique*, 2020/2 (Vol. 70), p. 337-363.
